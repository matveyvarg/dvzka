/**
 * Created by matveyvarg on 7/8/17.
 */
(function () {
    var $ = jQuery;
    $('.slider').slick({
        prevArrow:"<button type='button' class='slick-prev'></button>",
        nextArrow:"<button type='button' class='slick-next'></button>",
    });
    $('.slick-rooms').slick({
        prevArrow:"<button type='button' class='slick-prev'></button>",
        nextArrow:"<button type='button' class='slick-next'></button>",
    });
    $('input[name="since"]').datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $('input[name="till"]').datepicker({
        dateFormat: 'yy-mm-dd'
    });

})(jQuery);