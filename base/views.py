# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json

from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth import authenticate, login
from django.core import serializers
from django.http import JsonResponse

from base.models import *
# Create your views here.


def index(request):
    return render(request, 'base/index.html')


def services(request):
    servs = Services.objects.all()
    return render(request, 'base/services.html', {'servs': servs})


def service(request, id):
    return render(request, 'base/service.html',{'service': Services.objects.get(pk=id)})

def contacts(request):
    return render(request, 'base/contacts.html')


def booking(request):
    if request.method == "GET":
        return render(request, 'base/booking.html', {'rooms': Room.objects.all()})
    elif request.method == "POST":
        room = Room.objects.get(name=request.POST['name'])
        if not room.is_reserved(request.POST['since'], request.POST['till']):
            if not request.user.is_authenticated:
                return HttpResponse('Войдите на сайт!')
            room.booked_rooms.create(
                reserved_for=request.POST['name'],
                reserved_from=request.POST['since'],
                reserved_to=request.POST['till'],
                reserved_by=request.user
            )
            room.save()
            return HttpResponse('Номер успешно забронирован')
        else:
            return HttpResponse('Номер занят')
    else:
        return redirect('index')


def login_(request):
    if request.method == "POST":
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            login(request, user)
            return redirect('index')
    else:
        return redirect('index')


def rooms(request):
    rooms = Room.objects.all()
    return render(request, 'base/rooms.html', {'rooms': rooms})


def check_booking(request):
    booked_rooms = Room.objects.get(name=request.GET['name']).booked_rooms.all()
    response = {'rooms':json.loads(serializers.serialize('json',booked_rooms)),'user_id':request.user.id}
    return JsonResponse(response, safe=False)


def special(request):
    return render(request, 'base/special.html')

def about(request):
    return render(request, 'base/about.html')

def register(request):
    if request.method == 'GET':
        return render(request,'base/register.html')
    else:
        user = User.objects.create_user(username=request.POST['username'],
                                 email=request.POST['email'],
                                 password=request.POST['password'])
        if user is not None:
            user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
            if user is not None:
                login(request, user)
                return redirect('index')

    return redirect('index')
