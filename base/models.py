# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
# Create your models here.


def get_upload_path(instance, filename):
    return str(instance.pk) + '/' + filename


class Room(models.Model):
    name = models.CharField(max_length=128, null=False, default='Номер')
    price = models.IntegerField(null=False, default=0)
    description = models.TextField(blank=True, default='')

    def is_reserved(self, _from, _to):
        _from = datetime.strptime(_from, '%Y-%m-%d').date()
        _to = datetime.strptime(_to, '%Y-%m-%d').date()
        for booked in self.booked_rooms.all():
            if _from >= booked.reserved_from and _to <= booked.reserved_to:
                return True


class Booking(models.Model):
    room = models.ForeignKey(Room, related_name='booked_rooms', null=True, blank=True)
    reserved_for = models.CharField(max_length=255,null=True)
    reserved_from = models.DateField(null=True, blank=True)
    reserved_to = models.DateField(null=True, blank=True)
    reserved_by = models.ForeignKey(User, related_name='reserved_room', null=True, blank=True)


class Services(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField()
    image = models.ImageField(null=True, upload_to=get_upload_path)

    def __str__(self):
        return self.name
