"""Hotel URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from base import views
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.index, name='index'),
    url(r'^services/', include([
        url(r'^$', views.services, name='services'),
        url(r'^(?P<id>\d+)', views.service, name='service')
    ])),
    url(r'^contacts/$', views.contacts, name='contacts'),
    url(r'^booking/$', views.booking, name='booking'),
    url(r'^login_/$', views.login_, name='login_'),
    url(r'^rooms/$', views.rooms, name='rooms'),
    url(r'^check-booking/$', views.check_booking, name='check-booking'),
    url(r'special/$', views.special, name='special'),
    url(r'^about/$', views.about, name='about'),
    url(r'register/', views.register, name='register')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
